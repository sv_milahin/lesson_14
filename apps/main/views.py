from flask import Flask, g,  jsonify

from config.settings import DevelopmentConfig
from utils import get_db, DataBase

app = Flask(__name__)

app.config.from_object(DevelopmentConfig)


@app.route('/movie/<string:title>')
def movie_title_page(title):
    db = get_db()
    dbase = DataBase(db).get_title(title)
    list_key = ['title', 'country', 'release_year', 'genre', 'description']
    list_value = dbase
    movie = dict(zip(list_key, list_value))
    return jsonify(movie)


@app.route('/movie/<int:year1>/to/<int:year2>')
def movie_year_page(year1, year2):
    db = get_db()
    dbase = DataBase(db).get_year_to_year(year1, year2)
    list_key = ['title', 'release_year']
    movie = [dict(zip(list_key, k)) for k in dbase]
    return jsonify(movie)


@app.route('/rating/<string:name>')
def rating_children_page(name):
    db = get_db()
    dbase = DataBase(db).get_raiting(name)
    list_key = ['title', 'raiting', 'description']
    rating = [dict(zip(list_key, k)) for k in dbase]
    return jsonify(rating)


@app.route('/genre/<string:name>')
def genre_page(name):
    db = get_db()
    dbase = DataBase(db).get_genre(name)
    list_key = ['title', 'description']
    genre = [dict(zip(list_key, k)) for k in dbase]
    return jsonify(genre)


@app.route('/cast/<string:name1>/<string:name2>')
def cat_page(name1, name2):
    db = get_db()
    dbase = DataBase(db).get_cast(name1, name2)
    if dbase[1] > 2:
        data = dbase[0].split()
        return jsonify({'cast': ' '.join(data[4:])})
    return jsonify({'cast': 'No'})


@app.route('/<string:type_>/<string:year>/<string:genre>')
def type_year_genre_page(type_, year, genre):
    db = get_db()
    dbase = DataBase(db).get_type_year_genre(type_, year, genre)
    list_key = ['title', 'description']
    date = [dict(zip(list_key, k)) for k in dbase]
    return jsonify(date)


@app.teardown_appcontext
def close_db(error):
    if hasattr(g, 'link_db'):
        g.link_db.close()
