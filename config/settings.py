from pathlib import Path


class BaseConfig:
    DEBAG = True
    BASE_DIR: Path = Path(__file__).parents[1]
    DATABASE: str = f'{BASE_DIR}/data/netflix.db'


class DevelopmentConfig(BaseConfig):
    pass


class TestingConfig(BaseConfig):
    pass


class ProductionConfig(BaseConfig):
    DEBAG = False
