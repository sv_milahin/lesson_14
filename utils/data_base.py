from loguru import logger


from config.settings import BaseConfig


logger.add(f'{BaseConfig.BASE_DIR}/logs/db.log', format='{time} {level} {message}', level='INFO')


class DataBase:
    def __init__(self, db):
        """

        :param db:
        :return:
        """
        self.__db = db
        self.__cur = db.cursor()

    def get_title(self, title):
        """

        :return:
        """
        sql = f'''
                    SELECT title, country, release_year, listed_in, description 
                    FROM netflix 
                    WHERE title = '{title}'
                    ORDER BY release_year DESC
                    
                    '''
        logger.info(f'Запрос: title')
        try:
            self.__cur.execute(sql)
            res = self.__cur.fetchone()

            if res:
                return res
        except Exception as error:
            logger.info(f'Не удалось загрузить: {error}')
        return []

    def get_year_to_year(self, year1, year2):
        """

        :return:
        """
        sql = f'''
                    SELECT title, release_year 
                    FROM netflix 
                    WHERE release_year IN ({int(year1)}, {int(year2)})
                    ORDER BY release_year DESC
                    LIMIT 100
                    '''
        logger.info(f'Запрос: year')
        try:
            self.__cur.execute(sql)
            res = self.__cur.fetchall()

            if res:
                return res
        except Exception as error:
            logger.info(f'Не удалось загрузить: {error}')
        return []

    def get_raiting(self, name):
        """

        :return:
        """
        if name == 'children':
            sql = f'''
                                SELECT title, rating, description
                                FROM netflix 
                                WHERE rating = 'G'
                                ORDER BY title DESC
                                LIMIT 100
                                '''
            logger.info(f'Запрос: children')
        elif name == 'family':
            sql = f'''
                                SELECT title, rating, description
                                FROM netflix 
                                WHERE rating IN ('G', 'PG', 'PG-13')
                                ORDER BY title DESC
                                LIMIT 100
                                '''
            logger.info(f'Запрос: family')
        elif name == 'adult':
            sql = f'''
                                SELECT title, rating, description
                                FROM netflix 
                                WHERE rating IN ('R', 'NC-17')
                                ORDER BY title DESC
                                LIMIT 100
                                '''
            logger.info(f'Запрос: adult')
        else:
            logger.info(f'Запрос: other')
            pass
        try:
            self.__cur.execute(sql)
            res = self.__cur.fetchall()
            print
            if res:
                return res
        except Exception as error:
            logger.info(f'Не удалось загрузить: {error}')
        return []

    def get_genre(self, name):
        """

        :return:
        """
        sql = f'''
                    SELECT title, description
                    FROM netflix 
                    WHERE  listed_in LIKE '%{name}%'
                    ORDER BY release_year DESC 
                    LIMIT 10 
                    '''
        logger.info(f'Запрос: genre')
        try:
            self.__cur.execute(sql)
            res = self.__cur.fetchall()

            if res:
                return res
        except Exception as error:
            logger.info(f'Не удалось загрузить: {error}')
        return []

    def get_cast(self, name1, name2):
        """

        :return:
        """

        sql = f'''
                SELECT `cast`, count(`cast`)
                FROM netflix 
                WHERE `cast`  LIKE '%{name1}%' AND `cast`LIKE '%{name2}%'
                          
            '''
        logger.info(f'Запрос: cast')
        try:
            self.__cur.execute(sql)
            res = self.__cur.fetchone()
            if res:
                return res
        except Exception as error:
            logger.info(f'Не удалось загрузить: {error}')
        return []

    def get_type_year_genre(self, type_, year, genre):
        """

        :return:
        """

        sql = f'''
                SELECT title, description
                FROM netflix 
                WHERE `type` LIKE '%{type_}%' AND release_year LIKE '%{year}%' AND listed_in LIKE '%{genre}%'
                ORDER BY release_year
            '''
        logger.info(f'Запрос: type, year, genre')
        try:
            self.__cur.execute(sql)
            res = self.__cur.fetchall()
            if res:
                return res
        except Exception as error:
            logger.info(f'Не удалось загрузить: {error}')
        return []
