import sqlite3

from flask import g
from loguru import logger

from config.settings import BaseConfig

logger.add(f'{BaseConfig.BASE_DIR}/logs/db.log', format='{time} {level} {message}', level='INFO')


def connect_db():
    try:
        conn = sqlite3.connect(BaseConfig.DATABASE)

        logger.info('Загрузка прошла!')
        return conn
    except sqlite3.Error as error:
        logger.info(f'Не удалось загрузить БД: {error}')


def get_db():
    if not hasattr(g, 'link_db'):
        g.link_db = connect_db()
    return g.link_db
